<?php
/**
* Template Name: Home Page - Main Landing
*
* @package regel
* @subpackage regel-mk01-theme
* @since Mk. 1.0
*/
?>

<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php /* MAIN HERO CONTAINER */ ?>
        <section class="main-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-hero-content-left col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <?php the_content(); ?>
                    </div>
                    <div class="main-hero-content-right col-xl-6 offset-xl-1 col-lg-6 offset-lg-1 col-md-6 col-sm-12 col-12">
                        <?php the_post_thumbnail('full', array('class' => 'img-fluid img-main-hero'))?>
                    </div>
                </div>
            </div>
        </section>
        <?php /* MAIN ABOUT CONTAINER */ ?>
        <section class="main-about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php /* FIRST SECTION */ ?>
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-about-content-left col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'rgl_about_text_1', true)); ?>
                    </div>
                    <div class="main-about-content-right col-xl-5 offset-xl-1 col-lg-5 offset-lg-1 col-md-6 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'rgl_about_image_1_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </div>
                </div>
            </div>
            <?php /* SECOND SECTION */ ?>
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-about-content-left left-2 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'rgl_about_text_2', true)); ?>
                    </div>
                    <div class="main-about-content-right right-2 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'rgl_about_image_2_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                    </div>

                </div>
            </div>
        </section>
        <?php /* MAIN INFOGRAPHIC CONTAINER */ ?>
        <section class="main-info-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-top justify-content-center">
                    <div class="main-info-content-title title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'rgl_info_main_title', true); ?></h2>
                    </div>
                    <div class="w-100"></div>
                    <?php $arr_info = get_post_meta(get_the_ID(), 'rgl_info_group', true); ?>
                    <?php if (!empty($arr_info)) { ?>
                    <?php $i = 1; ?>
                    <?php foreach ($arr_info as $item) { ?>
                    <div class="main-info-item col-xl col-lg col-md col-sm-6 col-12">
                        <div class="main-info-wrapper">
                            <div class="main-info-number">
                                <span><?php echo $i; ?></span>
                            </div>
                            <div class="main-info-content">
                                <?php echo $item['desc']; ?>
                            </div>
                            <div class="main-info-image">
                                <?php $bg_banner_id = $item['image_id']; ?>
                                <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                                <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <?php } ?>
                    <div class="w-100"></div>
                    <div class="main-info-content-posttitle col-xl-8 col-lg-8 col-md-10 col-sm-12 col-12">
                        <div class="main-info-posttitle-arrow"></div>
                        <h2><?php echo get_post_meta(get_the_ID(), 'rgl_info_post_title', true); ?></h2>
                    </div>
                </div>
            </div>
        </section>
        <?php /* MAIN TECHNOLOGY CONTAINER */ ?>
        <?php /*?>
        <section class="main-tech-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="main-tech-content-title title-container col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'rgl_tech_main_title', true); ?></h2>
                    </div>
                    <?php $arr_tech = get_post_meta(get_the_ID(), 'rgl_tech_group', true); ?>
                    <?php if (!empty($arr_tech)) { ?>
                    <?php $i = 1; ?>
                    <?php foreach ($arr_tech as $item) { ?>
                    <div class="main-tech-item col-xl col-lg col-md col-sm-6 col-12">
                        <div class="main-tech-item-wrapper">
                            <div class="main-tech-number">
                                <span><?php echo $i; ?></span>
                            </div>
                            <div class="main-tech-content">
                                <?php echo $item['desc']; ?>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php */ ?>
        <?php /* MAIN SLOGAN CONTAINER */ ?>
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'rgl_slogan_image_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-slogan-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="main-slogan-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'rgl_slogan_text', true)); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php /* MAIN TEAM CONTAINER */ ?>
        <section class="main-team-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center justify-content-start">
                    <div class="main-team-title-container title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'rgl_team_main_title', true); ?></h2>
                    </div>
                    <?php $arr_team = new WP_Query(array('post_type' => 'team', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array(array('taxonomy' => 'team_type', 'field' => 'slug', 'terms' => 'board')))); ?>
                    <?php if ($arr_team->have_posts()) : ?>
                    <?php $i = 1; ?>
                    <?php while ($arr_team->have_posts()) : $arr_team->the_post(); ?>
                    <?php if ($i == 1) { ?>
                    <div class="team-item col-xl-2 col-lg-2 col-md-3 col-sm-3 col-12">
                        <div class="team-item-wrapper">
                            <div class="team-item-sub-title">
                                <?php _e('Board', 'regel'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <?php get_template_part('templates/template-team-item'); ?>
                    <?php } ?>
                    <?php $i++; endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <div class="w-100"></div>
                    <?php $arr_team = new WP_Query(array('post_type' => 'team', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array(array('taxonomy' => 'team_type', 'field' => 'slug', 'terms' => 'advisors')))); ?>
                    <?php if ($arr_team->have_posts()) : ?>
                    <?php $i = 1; ?>
                    <?php while ($arr_team->have_posts()) : $arr_team->the_post(); ?>
                    <?php if ($i == 1) { ?>
                    <div class="team-item col-xl-2 col-lg-2 col-md-3 col-sm-3 col-12">
                        <div class="team-item-wrapper">
                            <div class="team-item-sub-title">
                                <?php _e('Advisors', 'regel'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <?php get_template_part('templates/template-team-item'); ?>
                    <?php } ?>
                    <?php $i++; endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <div class="w-100"></div>
                    <?php $arr_team = new WP_Query(array('post_type' => 'team', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array(array('taxonomy' => 'team_type', 'field' => 'slug', 'terms' => 'operation')))); ?>
                    <?php if ($arr_team->have_posts()) : ?>
                    <?php $i = 1; ?>
                    <?php while ($arr_team->have_posts()) : $arr_team->the_post(); ?>
                    <?php if ($i == 1) { ?>
                    <div class="team-item col-xl-2 col-lg-2 col-md-3 col-sm-3 col-12">
                        <div class="team-item-wrapper">
                            <div class="team-item-sub-title">
                                <?php _e('Operation', 'regel'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } else { ?>
                    <?php get_template_part('templates/template-team-item'); ?>
                    <?php } ?>
                    <?php $i++; endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                    <div class="w-100"></div>
                </div>
            </div>
        </section>
        <?php /* MAIN WORKS CONTAINER */ ?>
        <section class="main-work-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="main-work-title-container title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'rgl_work_main_title', true); ?></h2>
                    </div>
                    <div class="main-work-description-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'rgl_work_text', true)); ?>
                    </div>
                    <div class="main-work-items-container col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <?php $arr_works = get_post_meta(get_the_ID(), 'rgl_works_group', true); ?>
                        <?php if (!empty($arr_works)) { ?>
                        <?php $i = 1; ?>
                        <div class="accordion custom-accordion" id="accordionWorks">
                            <?php foreach ($arr_works as $item) { ?>
                            <div class="card">
                                <div class="card-header" id="heading<?php echo $i; ?>">
                                    <h2 class="mb-0">
                                        <?php $class_btn = 'collapsed'; ?>
                                        <button class="btn btn-link btn-block text-left <?php echo $class_btn; ?>" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
                                            <?php echo $item['title']; ?>
                                            <span class="d-block"><?php echo $item['location']; ?></span>
                                            <div class="close-btn"></div>
                                        </button>
                                    </h2>
                                </div>
                                <?php $class_panel = ($i == 1) ? '' : ''; ?>
                                <div id="collapse<?php echo $i; ?>" class="collapse <?php echo $class_panel; ?>" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordionWorks">
                                    <div class="card-body">
                                        <?php echo apply_filters('the_content', $item['desc']); ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; } ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>