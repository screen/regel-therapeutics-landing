<div class="team-item col-xl-2 col-lg-2 col-md-3 col-sm-3 col-12">
    <div class="team-item-wrapper">
        <h4><?php the_title(); ?></h4>
        <div class="team-item-content">
            <?php the_content(); ?>
        </div>
    </div>
</div>