<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Members', 'Post Type General Name', 'regel' ),
		'singular_name'         => _x( 'Member', 'Post Type Singular Name', 'regel' ),
		'menu_name'             => __( 'Team', 'regel' ),
		'name_admin_bar'        => __( 'Team', 'regel' ),
		'archives'              => __( 'Team Archives', 'regel' ),
		'attributes'            => __( 'Member Attributes', 'regel' ),
		'parent_item_colon'     => __( 'Parent Member:', 'regel' ),
		'all_items'             => __( 'All Team Members', 'regel' ),
		'add_new_item'          => __( 'Add New Member', 'regel' ),
		'add_new'               => __( 'Add New', 'regel' ),
		'new_item'              => __( 'New Member', 'regel' ),
		'edit_item'             => __( 'Edit Member', 'regel' ),
		'update_item'           => __( 'Update Member', 'regel' ),
		'view_item'             => __( 'View Member', 'regel' ),
		'view_items'            => __( 'View Team', 'regel' ),
		'search_items'          => __( 'Search Member', 'regel' ),
		'not_found'             => __( 'Not found', 'regel' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'regel' ),
		'featured_image'        => __( 'Featured Image', 'regel' ),
		'set_featured_image'    => __( 'Set featured image', 'regel' ),
		'remove_featured_image' => __( 'Remove featured image', 'regel' ),
		'use_featured_image'    => __( 'Use as featured image', 'regel' ),
		'insert_into_item'      => __( 'Insert into Member', 'regel' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Member', 'regel' ),
		'items_list'            => __( 'Team list', 'regel' ),
		'items_list_navigation' => __( 'Team list navigation', 'regel' ),
		'filter_items_list'     => __( 'Filter Team list', 'regel' ),
	);
	$args = array(
		'label'                 => __( 'Member', 'regel' ),
		'description'           => __( 'Team Members', 'regel' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'team_type' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'custom_post_type', 0 );

// Register Custom Taxonomy
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Team Types', 'Taxonomy General Name', 'regel' ),
		'singular_name'              => _x( 'Team Type', 'Taxonomy Singular Name', 'regel' ),
		'menu_name'                  => __( 'Team Type', 'regel' ),
		'all_items'                  => __( 'All Team Type', 'regel' ),
		'parent_item'                => __( 'Parent Team Type', 'regel' ),
		'parent_item_colon'          => __( 'Parent Team Type:', 'regel' ),
		'new_item_name'              => __( 'New Team Type Name', 'regel' ),
		'add_new_item'               => __( 'Add New Team Type', 'regel' ),
		'edit_item'                  => __( 'Edit Team Type', 'regel' ),
		'update_item'                => __( 'Update Team Type', 'regel' ),
		'view_item'                  => __( 'View Team Type', 'regel' ),
		'separate_items_with_commas' => __( 'Separate Team Types with commas', 'regel' ),
		'add_or_remove_items'        => __( 'Add or remove Team Types', 'regel' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'regel' ),
		'popular_items'              => __( 'Popular Team Types', 'regel' ),
		'search_items'               => __( 'Search Team Types', 'regel' ),
		'not_found'                  => __( 'Not Found', 'regel' ),
		'no_terms'                   => __( 'No Team Types', 'regel' ),
		'items_list'                 => __( 'Team Types list', 'regel' ),
		'items_list_navigation'      => __( 'Team Types list navigation', 'regel' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'team_type', array( 'team' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );