<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}
/**
 * CMB2 Custom Metaboxes
 *
 * @link https://woocommerce.com/
 *
 * @package regel
 */
class customCMB2Landing extends customCMB2Class
{
    /**
     * Main Constructor.
     */
    public function __construct()
    {
        add_action('cmb2_admin_init', array($this, 'regel_register_landing_metabox'));
    }

    /**
     * Register custom metaboxes.
     */
    public function regel_register_landing_metabox()
    {
        /* ABOUT METABOX */
        $cmb_home_about = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'about_metabox',
            'title'         => esc_html__('Home: About Section', 'regel'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-main-landing.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));
        
        $cmb_home_about->add_field(array(
            'id'        => parent::PREFIX . 'about_text_1',
            'name'      => esc_html__('First Content', 'regel'),
            'desc'      => esc_html__('Enter the first content of this section', 'regel'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

        $cmb_home_about->add_field(array(
            'id'        => parent::PREFIX . 'about_image_1',
            'name'      => esc_html__('First Image', 'regel'),
            'desc'      => esc_html__('Upload the first image of this section', 'regel'),
            'type'      => 'file',
        
            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Upload Image', 'regel'),
            ),
            'query_args' => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));

        $cmb_home_about->add_field(array(
            'id'        => parent::PREFIX . 'about_text_2',
            'name'      => esc_html__('Second Content', 'regel'),
            'desc'      => esc_html__('Enter the Second content of this section', 'regel'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

        $cmb_home_about->add_field(array(
            'id'        => parent::PREFIX . 'about_image_2',
            'name'      => esc_html__('Second Image', 'regel'),
            'desc'      => esc_html__('Upload the Second image of this section', 'regel'),
            'type'      => 'file',
        
            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Upload Image', 'regel'),
            ),
            'query_args' => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));

        /* INFO METABOX */
        $cmb_home_info = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'info_metabox',
            'title'         => esc_html__('Home: Infographic Section', 'regel'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-main-landing.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_home_info->add_field(array(
            'id'        => parent::PREFIX . 'info_main_title',
            'name'      => esc_html__('Main Section Title', 'regel'),
            'desc'      => esc_html__('Enter the main title of this section', 'regel'),
            'type'      => 'text'
        ));

        $group_field_id = $cmb_home_info->add_field(array(
            'id'            => parent::PREFIX . 'info_group',
            'name'          => esc_html__('Info Items Group', 'regel'),
            'description'   => __('Info items inside section ', 'regel'),
            'type'          => 'group',
            'options'       => array(
                'group_title'       => __('Info {#}', 'regel'),
                'add_button'        => __('Add other info', 'regel'),
                'remove_button'     => __('Remove info', 'regel'),
                'sortable'          => true,
                'closed'            => true,
                'remove_confirm'    => esc_html__('are you sure to remove this item?', 'regel')
            )
        ));
        
        $cmb_home_info->add_group_field($group_field_id, array(
            'id'        => 'image',
            'name'      => esc_html__('Info item image', 'regel'),
            'desc'      => esc_html__('Upload an image for this item', 'regel'),
            'type'      => 'file',
            'options'   => array(
                'url'   => false
            ),
            'text'      => array(
                'add_upload_file_text' => esc_html__('Upload image', 'regel'),
            ),
            'query_args' => array(
                'type'  => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));
        
        $cmb_home_info->add_group_field($group_field_id, array(
            'id'        => 'desc',
            'name'      => esc_html__('Info item content', 'regel'),
            'desc'      => esc_html__('Enter the description for this item', 'regel'),
            'type'      => 'textarea_small'
        ));

        
        $cmb_home_info->add_field(array(
            'id'        => parent::PREFIX . 'info_post_title',
            'name'      => esc_html__('Section Post Title', 'regel'),
            'desc'      => esc_html__('Enter the post title of this section', 'regel'),
            'type'      => 'textarea_small'
        ));

        /* TECHNOLOGY METABOX */
        $cmb_home_tech = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'tech_metabox',
            'title'         => esc_html__('Home: Technology Section', 'regel'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-main-landing.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_home_tech->add_field(array(
            'id'        => parent::PREFIX . 'tech_main_title',
            'name'      => esc_html__('Main Section Title', 'regel'),
            'desc'      => esc_html__('Enter the main title of this section', 'regel'),
            'type'      => 'text'
        ));

        $group_field_id = $cmb_home_tech->add_field(array(
            'id'            => parent::PREFIX . 'tech_group',
            'name'          => esc_html__('Info Items Group', 'regel'),
            'description'   => __('Info items inside section ', 'regel'),
            'type'          => 'group',
            'options'       => array(
                'group_title'       => __('Info {#}', 'regel'),
                'add_button'        => __('Add other info', 'regel'),
                'remove_button'     => __('Remove info', 'regel'),
                'sortable'          => true,
                'closed'            => true,
                'remove_confirm'    => esc_html__('are you sure to remove this item?', 'regel')
            )
        ));
        
        $cmb_home_tech->add_group_field($group_field_id, array(
            'id'        => 'desc',
            'name'      => esc_html__('Info item content', 'regel'),
            'desc'      => esc_html__('Enter the description for this item', 'regel'),
            'type'      => 'textarea_small'
        ));

        /* SLOGAN METABOX */
        $cmb_home_slogan = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'slogan_metabox',
            'title'         => esc_html__('Home: Technology Section', 'regel'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-main-landing.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_home_slogan->add_field(array(
            'id'        => parent::PREFIX . 'slogan_image',
            'name'      => esc_html__('Background Image', 'regel'),
            'desc'      => esc_html__('Upload the Background image of this section', 'regel'),
            'type'      => 'file',
       
            'options' => array(
                'url' => false
            ),
            'text'    => array(
                'add_upload_file_text' => esc_html__('Upload Image', 'regel'),
            ),
            'query_args' => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png'
                )
            ),
            'preview_size' => 'thumbnail'
        ));

        $cmb_home_slogan->add_field(array(
            'id'        => parent::PREFIX . 'slogan_text',
            'name'      => esc_html__('Main Section Text', 'regel'),
            'desc'      => esc_html__('Enter the main text of this section', 'regel'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

        /* TEAM METABOX */
        $cmb_home_team = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'team_metabox',
            'title'         => esc_html__('Home: Team Section', 'regel'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-main-landing.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_home_team->add_field(array(
            'id'        => parent::PREFIX . 'team_main_title',
            'name'      => esc_html__('Main Section Title', 'regel'),
            'desc'      => esc_html__('Enter the main title of this section', 'regel'),
            'type'      => 'text'
        ));

         /* WORKS METABOX */
         $cmb_home_works = new_cmb2_box(array(
            'id'            => parent::PREFIX . 'works_metabox',
            'title'         => esc_html__('Home: Team Section', 'regel'),
            'object_types'  => array('page'),
            'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-main-landing.php'),
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true,
            'cmb_styles'    => true,
            'closed'        => false
        ));

        $cmb_home_works->add_field(array(
            'id'        => parent::PREFIX . 'work_main_title',
            'name'      => esc_html__('Main Section Title', 'regel'),
            'desc'      => esc_html__('Enter the main title of this section', 'regel'),
            'type'      => 'text'
        ));

        $cmb_home_works->add_field(array(
            'id'        => parent::PREFIX . 'work_text',
            'name'      => esc_html__('Main Section Title', 'regel'),
            'desc'      => esc_html__('Enter the main title of this section', 'regel'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

        $group_field_id = $cmb_home_works->add_field(array(
            'id'            => parent::PREFIX . 'works_group',
            'name'          => esc_html__('Works Items Group', 'regel'),
            'description'   => __('Works items inside section ', 'regel'),
            'type'          => 'group',
            'options'       => array(
                'group_title'       => __('Info {#}', 'regel'),
                'add_button'        => __('Add other info', 'regel'),
                'remove_button'     => __('Remove info', 'regel'),
                'sortable'          => true,
                'closed'            => true,
                'remove_confirm'    => esc_html__('are you sure to remove this item?', 'regel')
            )
        ));

        $cmb_home_works->add_group_field($group_field_id, array(
            'id'        => 'title',
            'name'      => esc_html__('Info item Title', 'regel'),
            'desc'      => esc_html__('Enter the title for this item', 'regel'),
            'type'      => 'text'
        ));

        $cmb_home_works->add_group_field($group_field_id, array(
            'id'        => 'location',
            'name'      => esc_html__('Info item Location', 'regel'),
            'desc'      => esc_html__('Enter the Location for this item', 'regel'),
            'type'      => 'text'
        ));
        
        $cmb_home_works->add_group_field($group_field_id, array(
            'id'        => 'desc',
            'name'      => esc_html__('Info item content', 'regel'),
            'desc'      => esc_html__('Enter the description for this item', 'regel'),
            'type'      => 'wysiwyg',
            'options'   => array(
                'textarea_rows' => get_option('default_post_edit_rows', 2),
                'teeny'         => false
            )
        ));

        $cmb_home_works->add_group_field($group_field_id, array(
            'id'        => 'link_url',
            'name'      => esc_html__('Info item Location', 'regel'),
            'desc'      => esc_html__('Enter the Location for this item', 'regel'),
            'type'      => 'text_url'
        ));
    }
}

new customCMB2Landing;