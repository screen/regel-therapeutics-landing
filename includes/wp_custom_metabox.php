<?php

if (!defined('ABSPATH')) {
    die('Invalid request.');
}
/**
 * CMB2 Custom Metaboxes
 *
 * @link https://woocommerce.com/
 *
 * @package regel
 */
class customCMB2Class
{
    const PREFIX = 'rgl_';
    /**
     * Main Constructor.
     */
    public function __construct()
    {
        add_filter('cmb2_show_on', array($this, 'ed_metabox_include_front_page'), 10, 2);
        add_filter('cmb2_show_on',  array($this, 'be_metabox_show_on_slug'), 10, 2);
    }

    /**
     * Include custom 'show_on' for Front Page
     */
    public function ed_metabox_include_front_page($display, $meta_box)
    {
        if (!isset($meta_box['show_on']['key'])) {
            return $display;
        }

        if ('front-page' !== $meta_box['show_on']['key']) {
            return $display;
        }

        $post_id = 0;

        // If we're showing it based on ID, get the current ID
        if (isset($_GET['post'])) {
            $post_id = $_GET['post'];
        } elseif (isset($_POST['post_ID'])) {
            $post_id = $_POST['post_ID'];
        }

        if (!$post_id) {
            return false;
        }

        // Get ID of page set as front page, 0 if there isn't one
        $front_page = get_option('page_on_front');

        // there is a front page set and we're on it!
        return $post_id == $front_page;
    }

    /**
     * Include custom 'show_on' for Page Slug
     */
    public function be_metabox_show_on_slug($display, $meta_box)
    {
        if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
            return $display;
        }

        if ('slug' !== $meta_box['show_on']['key']) {
            return $display;
        }

        $post_id = 0;

        // If we're showing it based on ID, get the current ID
        if (isset($_GET['post'])) {
            $post_id = $_GET['post'];
        } elseif (isset($_POST['post_ID'])) {
            $post_id = $_POST['post_ID'];
        }

        if (!$post_id) {
            return $display;
        }

        $slug = get_post($post_id)->post_name;

        // See if there's a match
        return in_array($slug, (array) $meta_box['show_on']['value']);
    }
}

// Initialize class
new customCMB2Class;

require_once('class-metabox-landing.php');